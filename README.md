### Django
- create django app  
- connect app to mysql backend

- containerize
- startup container
- validate that the app is running and working properly

- Put into a pipeline
- add a mysql service to the pipeline

### Application setup
- Create a project directory named `django_practice_project`
  ```
  mkdir django_practice_project
  ```
- Change into the `django_practice_project` directory
  ```
  cd django_practice_project
  ```
- Create the `myapp` virtual environment in project root
  ```
  python -m venv myapp
  ```
- Update pip
  ```
  pip install --update pip
  ```
- Install Django
  ```
  python -m pip install Django
  ```
- Create the Django project named `mysite`
  ```
  django-admin startproject mysite
  ```
- Change to the `mysite/` directory and start the development server
  ```
  cd mysite
  python manage.py runserver
  ```
- Verify that the development server is running by navigating to [http://localhost:8000](http://localhost:8000)
- Stop the development server by pressing `CTRL + C` in the same terminal that it is running in

### Containerize the application
- Create a new directory named `docker` in the `django_practice_project` directory
- Create a new file named `dockerfile-django` in the `docker` directory
- Add the following code to the `dockerfile-django` file:
  ```docker
  ARG REGISTRY=docker.io
  ARG PYTHON_VERSION=slim-bullseye

  FROM ${REGISTRY}/python:${PYTHON_VERSION}

  WORKDIR /usr/src/app
  ENV PYTHONDONTWRITEBYTECODE=1
  ENV PYTHONUNBUFFERED=1
  RUN apt-get update
  RUN apt-get install -y python3-dev default-libmysqlclient-dev build-essential pkg-config

  COPY myapp/requirements.txt ./
  RUN pip install --no-cache-dir -r requirements.txt
  RUN apt update
  RUN apt install iputils-ping default-mysql-client -y

  COPY myapp/mysite .
  ```
- Create a new filed named `compose.yaml` in the `docker` directory
- Add the following code to the `compose.yaml` file:
  ```yaml
  version: '3.9'

  services:
    django-backend:
      build:
        context: ..
        dockerfile: Docker/dockerfile-django
      ports:
        - "8000:8000"
      expose:
        - 8000
  ```
- Ensure that you are in the `docker` directory and that a docker daemon (ie Docker Desktop) is running
- Build the application image and start a container running the image:
  ```
  docker compose up -d --build
  ```
- Verify that the development server image is running on port 8000 inside the container by navigating to [http://localhost:8000](http://localhost:8000)