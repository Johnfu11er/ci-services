- if there is a tag
  - heml version is the tag
  - app version is the tag

- Default branch
  - helm version is     0.1.x, commit count on default branch
                        "0.1.$CI_PIPELINE_IID"
  - app version is      "commit.$CI_COMMIT_SHORT_SHA"

- not default branch
  - helm version is     "0.0.1-dev-$CI_COMMIT_SHORT_SHA"
  - app version is      "commit.$CI_COMMIT_SHORT_SHA"
