- Done: make a shell entrypoint script
- Done: execute the entrypoint script from the `ENTRYPOINT` executable
- Done: Containerize the backend and mysql images with a version tag
- Done: push backend and mysql images to kind
- Create a deployment for the mysql in k8s
  - add a healthcheck to the mysql deployment / service
- create a deployment for the backend in k8s
  - Add a healthcheck to the backend that depends on the mysql deployment service being available and ready
- create a service for the backend in k8s
- create an ingress for the backend in k8s

### Path to IL4 ###
- Register application domain name
- Get app to a deployable state
  - App
    - Frontend
    - Backend
  - Support structure
    - Keycloak
    - Oauth2Proxy
    - NGINX
- Local testing and development of deployment
  - Craete images
  - Create helm chart with templates:
    - frontend deployment/service
    - backend deploymnet/service
    - oauth2proxy deployment/service
    - ingress
    - configmap
  - Deploy application into local kind cluster using helm chart
- GitLab CICD pipeline
  - Create frontend and backend images and store in GitLab container registry
  - Create Helm chart and store in GitLab container registry
  - Incorporate any testing structure that the team builds for frontend/backend
- IL4 deployment
  - Provision static resources
    - S3 bucket
    - Postgres RDS
  - Work with platform team
    - Additional EKS annotations
    - NGINX configuration
    - Keycloak client creation and integration
    - Certificates

### Other
- Integrate with SOFChat

- helm chart writing to the container registry
  - G8
- CPT Taptick app url registration
- keycloak integration
  - llp
  - g8
  - ravens eye
  